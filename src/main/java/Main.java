import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class that implements pagination of elements
 *
 * @author Tymofii Kirkicha
 * @version 1.0
 * @email timofyi.kirkicha77@gmail.com
 */
public class Main {
    public static void main(String[] args) {
        final int requiredPage = 2;     // <-- Try to request different pages
        final int paginationSize = 3;   // <-- Change these pagination settings

        // Different types of collections
//        List<Integer> elements = Collections.emptyList();
//        List<Integer> elements = Collections.singletonList(1);
//        List<Integer> elements = Arrays.asList(1, 2);
//        List<Integer> elements = Arrays.asList(1, 2, 3);
        List<Integer> elements = Arrays.asList(1, 2, 3, 4);
//        List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5);

        int pageCount = countPages(elements, paginationSize);

        System.out.printf("Count pages:%2d, pagination size:%2d, required page:%2d\n", pageCount, paginationSize, requiredPage);

        List<Integer> elementsOfPage = getElementsOfPage(elements, paginationSize, requiredPage);
        printArray(elementsOfPage);
    }

    // Simply printing of results
    private static <T> void printArray(Collection<T> result) {
        System.out.println("Result:");
        result.forEach(System.out::println);
    }

    /**
     * Method returns all elements from required paginated page
     *
     * @param elements       Collection of elements that will be paginated
     * @param paginationSize Number of elements in one pagination unit
     * @param requiredPage   Number of required page
     * @param <T>            Type of collection elements
     * @return All elements from required paginated page
     * @throws RuntimeException When required page out of bound of all paginated elements
     */
    public static <T> List<T> getElementsOfPage(List<T> elements, int paginationSize, int requiredPage) throws RuntimeException {
        if (elements == null || requiredPage <= 0 || paginationSize < 1) {
            throw new IllegalArgumentException();
        }

        List<T> result = new ArrayList<>();
        int arraySize = elements.size();

        int firstIndex = requiredPage * paginationSize - paginationSize + 1;
        int lastIndex = firstIndex + paginationSize;

        if (firstIndex == arraySize) {
            result.add(elements.get(arraySize - 1));
            return result;
        }

        if (firstIndex < arraySize) {
            if (lastIndex > arraySize) {
                for (int i = firstIndex; i <= arraySize; i++) {
                    T elementOfIndex = elements.get(i - 1);
                    result.add(elementOfIndex);
                }
            } else {
                for (int i = firstIndex; i < lastIndex; i++) {
                    T elementOfIndex = elements.get(i - 1);
                    result.add(elementOfIndex);
                }
            }
        } else {
            throw new RuntimeException("Page not exist");
        }
        return result;
    }

    /**
     * This method calculates the pages that will be available for pagination
     *
     * @param elements       Collection of elements that will be paginated
     * @param paginationSize Number of elements in one pagination unit
     * @param <T>            Type of collection elements
     * @return Number of elements for pagination
     */
    public static <T> int countPages(List<T> elements, int paginationSize) {
        if (elements == null || paginationSize < 1) {
            throw new IllegalArgumentException();
        }

        int arraySize = elements.size();
        int pageCount;

        if (arraySize < paginationSize) {
            if (arraySize == 0) {
                pageCount = 0;
            } else {
                pageCount = 1;
            }
        } else {
            if (arraySize % paginationSize == 0) {
                pageCount = arraySize / paginationSize;
            } else {
                pageCount = arraySize / paginationSize + 1;
            }
        }
        return pageCount;
    }
}
